#include <stdio.h>
#include <iostream>

using namespace std;

void algoritmo1(int n);
void algoritmo2(int n);
void algoritmo3(int n);

int main() {
	
	
	
	for(int i=2; i<=80; i+=2){
	
		//algoritmo1(i);
		//algoritmo2(i);
		algoritmo3(i);

	}
	
	cin.get();
	return 1;
}

void algoritmo1() {
	
	int n, i, j;
	int *a;
	cin >> n;
	
	for(i=0; i<n; i++) {
		
		a = new int[n];
		for(j=0; j<n; j++) {
			
			a[j] = i*j;
			cout<< a[j]<< endl;
		}
		delete[] a;
	}	
}

void algoritmo2(int n) {
	
	int j, i;
	int **a;
	
	int tamanio=0;
	
	cout << "n = "<<n<<endl;
	tamanio += sizeof(n);
	a = new int *[n];
	tamanio += sizeof(a);
	
	for(i=0; i<n; i++) {
		
		a[i] = new int[n];
		tamanio += sizeof(a[i]);
		for(j=0; j<n; j++) {
			
			a[i][j] = i*j;
			tamanio += sizeof(a[i][j]);
		}
	}
	
	for(i=0; i<n; i++) {
		
		for(j=0; j<n; j++) {
			
			//cout << a[i][j];	
		}
		
		delete[] a[i];
	}
	delete[] a;
	
	cout << "\nSizeof total: "<<tamanio<<"\n\n";
}

void algoritmo3(int n) {
	
	struct nodo {
		int x,y;
		nodo *sig;
	};
	
	int tamanio=0;
	cout << "n = "<<n<<endl;
	tamanio += sizeof(n);
	
	int i;
	nodo *cab = NULL, *aux;
	tamanio += sizeof(aux);
	tamanio += sizeof(cab);
	
	for(i=0; i<n; i++){
		
		aux = new nodo;
		aux->x = i;
		tamanio += sizeof(aux->x);
		aux->y = n-i;
		tamanio += sizeof(aux->y);
		aux->sig = cab;
		tamanio += sizeof(aux->sig);
		cab = aux;
	}
	
	while(aux != NULL) {
		
		//cout << aux->x << aux->y<<endl;
		aux = aux->sig;
	}
	
	for(aux=cab->sig; aux!=NULL; aux=aux->sig) {
		
		delete cab;
		cab = aux;
	}
	
	delete cab;
	cout<<"Sizeof total: "<< tamanio<<"\n\n";
}
